PROJECT = SERVO_LIB_TEST
DEBUG = yes

DEBUGLOG_LVL = 0x3# 0x0 - turn off debug log, 0x0 < x < 0x4 - avalible debug levels  
DEBUGLOG_OUT=stdout# (printf)/xitoa (xprintf) / RTT, or comment if custom c file.***Watch out for spaces after "="!
TESTING = no

OS=USE_NOOS#USE_NULLOS#USE_FREERTOS

COMMON_LIBS = ./../../Common_Libs
THIRD_PARTY = ./../../third_party

include $(COMMON_LIBS)/Commons.mk
include ./Drivers.mk
include ./test/Rules.mk

CPPSRC += src/main.cpp

CSRC += mock/mock_timer.c \
mock/gpio_lib_dummy.c

INCDIR += inc \
mock

ULIBS += -pthread -lutest

##############################################################################
# Compiler settings
#
GIT_VERSION := $(shell git describe --abbrev=4 --dirty --always --tags)

C_DEFS += -DVERSION=\"$(GIT_VERSION)\" 
CXX_DEFS += -DVERSION=\"$(GIT_VERSION)\"

ifneq ($(OS),)
C_DEFS += -D$(OS)
CXX_DEFS += -D$(OS)
endif

ifneq ($(DEBUGLOG_LVL),)
C_DEFS += -DLOG_LEVEL=$(DEBUGLOG_LVL)
CXX_DEFS += -DLOG_LEVEL=$(DEBUGLOG_LVL)

ifneq ($(DEBUGLOG_OUT),)
C_DEFS += -DLOG_OUT=$(DEBUGLOG_OUT)
CXX_DEFS += -DLOG_OUT=$(DEBUGLOG_OUT)
endif
endif

ifeq ($(DEBUG),yes)
C_DEFS += -DDEBUG_ENABLE -DUSE_FULL_ASSERT
CXX_DEFS += -DDEBUG_ENABLE -DUSE_FULL_ASSERT 

OPT_LVL = -O0
else
OPT_LVL = -O2
endif


ifeq ($(TESTING),yes)
C_DEFS += -DTEST_ENABLE
CXX_DEFS += -DTEST_ENABLE 
endif


# Define C warning options here
CWARN = -Wall -Wextra -pedantic -Wstrict-prototypes -Werror=implicit-function-declaration -Werror=incompatible-pointer-types
# Define C++ warning options here
CPPWARN = -Wall -Wextra -pedantic -Werror=implicit-function-declaration -Werror=incompatible-pointer-types

# C language standard ("c89" / "iso9899:1990", "iso9899:199409",
# "c99" / "iso9899:1999", "gnu89" - default, "gnu99")
C_STD = gnu99
# C++ language standard ("c++98", "gnu++98" - default, "c++0x", "gnu++0x")
CXX_STD = gnu++0x

# Compiler options
USE_OPT = $(OPT_LVL) -ggdb3 -fomit-frame-pointer -falign-functions=16

# C specific options (added to USE_OPT).
USE_COPT = -std=$(C_STD)
# C++ specific options (added to USE_OPT).
#USE_CPPOPT = -std=$(CXX_STD) -fno-rtti -fno-exceptions -fno-unwind-tables

# ARM-specific options
AOPT =

TRGT = 
CPPC = $(TRGT)g++
#LD   = $(TRGT)gcc
# Enable loading with g++ only if you need C++ runtime support.
# NOTE: You can use C++ even without C++ support if you are careful. C++
#       runtime support makes code size explode.
LD   = $(TRGT)g++
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp
OD   = $(TRGT)objdump
SZ   = $(TRGT)size
BIN  = $(CP) -O binary

# Verbose log while compiling
USE_VERBOSE_COMPILE = no

##############################################################################
# Linker settings
#

# Linker garbage collection: ask linker to remove unused code and data
USE_LINK_GC = yes

# Linker extra options
USE_LDOPT = 

# Link time optimizations (LTO)
USE_LTO = no

# ARM Cortex-Mx common makefile scripts and rules.

##############################################################################
# Processing options coming from the upper Makefile.
#

# Compiler options
OPT = $(USE_OPT)
COPT = $(USE_COPT)
CPPOPT = $(USE_CPPOPT)

# Garbage collection
ifeq ($(USE_LINK_GC),yes)
  OPT += -ffunction-sections -fdata-sections -fno-common
  LDOPT := ,--gc-sections
else
  LDOPT :=
endif

# Linker extra options
ifneq ($(USE_LDOPT),)
  LDOPT := $(LDOPT),$(USE_LDOPT)
endif

# Link time optimizations
ifeq ($(USE_LTO),yes)
  OPT += -flto
endif

# FPU-related options
ifeq ($(USE_FPU),)
  USE_FPU = no
endif
ifneq ($(USE_FPU),no)
    OPT += -fsingle-precision-constant
endif

# Output directory and files
ifeq ($(BUILDDIR),)
  BUILDDIR = build
endif
ifeq ($(BUILDDIR),.)
  BUILDDIR = build
endif
OUTFILES = $(BUILDDIR)/$(PROJECT)

# Source files groups and paths
  ACSRC += $(CSRC)
  ACPPSRC += $(CPPSRC)

ASRC	  = $(ACSRC) $(ACPPSRC)
SRCPATHS  = $(sort $(dir $(ASMXSRC)) $(dir $(ASMSRC)) $(dir $(ASRC)))

# Various directories
OBJDIR    = $(BUILDDIR)/obj
LSTDIR    = $(BUILDDIR)/lst

# Object files groups
ACOBJS    = $(addprefix $(OBJDIR)/, $(notdir $(ACSRC:.c=.o)))
ACPPOBJS  = $(addprefix $(OBJDIR)/, $(notdir $(ACPPSRC:.cpp=.o)))
ASMOBJS   = $(addprefix $(OBJDIR)/, $(notdir $(ASMSRC:.s=.o)))
ASMXOBJS  = $(addprefix $(OBJDIR)/, $(notdir $(ASMXSRC:.S=.o)))
OBJS	  = $(ASMXOBJS) $(ASMOBJS) $(ACOBJS) $(ACPPOBJS)

# Paths
IINCDIR   = $(patsubst %,-I%,$(INCDIR) $(DINCDIR) $(UINCDIR))
LLIBDIR   = $(patsubst %,-L%,$(DLIBDIR) $(ULIBDIR))

# Macros
DEFS      = $(DDEFS) $(UDEFS)
ADEFS 	  = $(DADEFS) $(UADEFS)

# Libs
LIBS      = $(DLIBS) $(ULIBS)

# Various settings
ODFLAGS	  = -x --syms
ASFLAGS   = $(MCFLAGS) -Wa,-amhls=$(LSTDIR)/$(notdir $(<:.s=.lst)) $(ADEFS)
ASXFLAGS  = $(MCFLAGS) -Wa,-amhls=$(LSTDIR)/$(notdir $(<:.S=.lst)) $(ADEFS)
CFLAGS    = $(MCFLAGS) $(OPT) $(COPT) $(CWARN) -Wa,-alms=$(LSTDIR)/$(notdir $(<:.c=.lst)) $(DEFS) $(C_DEFS)
CPPFLAGS  = $(MCFLAGS) $(OPT) $(CPPOPT) $(CPPWARN) -Wa,-alms=$(LSTDIR)/$(notdir $(<:.cpp=.lst)) $(DEFS) $(CXX_DEFS)
#LDFLAGS   = $(MCFLAGS) $(OPT) -nostartfiles $(LLIBDIR) -Wl,-Map=$(BUILDDIR)/$(PROJECT).map,--cref,--no-warn-mismatch,--script=$(LDSCRIPT)$(LDOPT)
LDFLAGS   = $(MCFLAGS) $(OPT) $(LLIBDIR) -Wl,-Map=$(BUILDDIR)/$(PROJECT).map,--cref,--no-warn-mismatch

# Generate dependency information
ASFLAGS  += -MD -MP -MF .dep/$(@F).d
CFLAGS   += -MD -MP -MF .dep/$(@F).d
CPPFLAGS += -MD -MP -MF .dep/$(@F).d

# Paths where to search for sources
VPATH     = $(SRCPATHS)

#
# Makefile rules
#

all: $(OBJS) $(OUTFILES) MAKE_ALL_RULE_HOOK

MAKE_ALL_RULE_HOOK:

$(OBJS): | $(BUILDDIR)

$(BUILDDIR) $(OBJDIR) $(LSTDIR):
ifneq ($(USE_VERBOSE_COMPILE),yes)
	@echo Compiler Options
	@echo $(CC) -c $(CFLAGS) -I. $(IINCDIR) main.c -o main.o
	@echo
endif
	mkdir -p $(OBJDIR)
	mkdir -p $(LSTDIR)

$(ACPPOBJS) : $(OBJDIR)/%.o : %.cpp Makefile
ifeq ($(USE_VERBOSE_COMPILE),yes)
	@echo
	$(CPPC) -c $(CPPFLAGS) $(AOPT) -I. $(IINCDIR) $< -o $@
else
	@echo Compiling $(<F)
	@$(CPPC) -c $(CPPFLAGS) $(AOPT) -I. $(IINCDIR) $< -o $@ 
endif

$(ACOBJS) : $(OBJDIR)/%.o : %.c Makefile
ifeq ($(USE_VERBOSE_COMPILE),yes)
	@echo
	$(CC) -c $(CFLAGS) $(AOPT) -I. $(IINCDIR) $< -o $@
else
	@echo Compiling $(<F)
	@$(CC) -c $(CFLAGS) $(AOPT) -I. $(IINCDIR) $< -o $@
endif

$(ASMOBJS) : $(OBJDIR)/%.o : %.s Makefile
ifeq ($(USE_VERBOSE_COMPILE),yes)
	@echo
	$(AS) -c $(ASFLAGS) -I. $(IINCDIR) $< -o $@
else
	@echo Compiling $(<F)
	@$(AS) -c $(ASFLAGS) -I. $(IINCDIR) $< -o $@
endif

$(ASMXOBJS) : $(OBJDIR)/%.o : %.S Makefile
ifeq ($(USE_VERBOSE_COMPILE),yes)
	@echo
	$(CC) -c $(ASXFLAGS) $(TOPT) -I. $(IINCDIR) $< -o $@
else
	@echo Compiling $(<F)
	@$(CC) -c $(ASXFLAGS) $(TOPT) -I. $(IINCDIR) $< -o $@
endif

$(OUTFILES): $(OBJS) $(LDSCRIPT)
ifeq ($(USE_VERBOSE_COMPILE),yes)
	@echo
	$(LD) $(OBJS) $(LDFLAGS) $(LIBS) -o $@
else
	@echo Linking $@
	@$(LD) $(OBJS) $(LDFLAGS) $(LIBS) -o $@
endif

clean:
	@echo Cleaning
	-rm -fR .dep $(BUILDDIR)
	@echo
	@echo Done

#
# Include the dependency files, should be the last of the makefile
#
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)

# *** EOF ***


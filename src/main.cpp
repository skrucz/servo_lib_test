/*
 ============================================================================
 Name        : servolib_dev.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Program for developing servo lib for MCUs.
 ============================================================================
 */

#include "common.h"
#include "main.hpp"
#include "servoTestCases.hpp"

static uint32_t relative_time;
static uint32_t system_tick = 0;
tev_dispatcher tevd_system = NULL;

/**
 * \brief Main function of test application.
 *
 * Application is dedicated for PC.
 * Used:
 * uTest framework. Copyright (c) 2017, Tymoteusz Blazejczyk
 * BSD 3-Clause License
 *
 */

enum app_timers {
  APP_TIM_SERVO = 0
};
#define SERVOD_TEST_CCR_NUM 1

SERVOD_CREATE_STATIC_INSTANCE(test, 4);

void
mock_timer_cmp_handler (struct mock_timer* timer, uint32_t ccr_num)
{
  UNUSED(timer);
  if (ccr_num == SERVOD_TEST_CCR_NUM) {
    servod_isr_handler(servod_test);
  }
}

void
mock_timer_arr_handler (struct mock_timer* timer)
{
  UNUSED(timer);
}

void*
threadSystemTick_1ms (void* value)
{
  UNUSED(value);

  for (;;) {
    system_tick++;
    if ((system_tick % 5000) == 0) {
	printf("\r\nSYS_TICK: %03d", system_tick/1000);
    }
    usleep(1 * 1000); //sleep for 1ms
  }

  return NULL;
}

int
main (int argc, char* argv[]) {
  UNUSED(argc); UNUSED(argv);

  int i = 0;
  relative_time = time(NULL);

  puts("!!!Servo_lib test application!!!");
  DEBUGSTR_LOG(1, "Debug log enabled");

  pthread_t threadSystemTick;
  if (pthread_create(&threadSystemTick, NULL, &threadSystemTick_1ms, NULL) != 0) {
    puts("\r\nFailed to create the thread");
    return 1;
  }
  tev_init_dispatcher(&tevd_system, &system_tick, TEV_USE_DYNAMIC_ALOC);

  if (servo_test_case() == EXIT_FAILURE) {
    std::cout << "All tests must PASS!";
    return EXIT_FAILURE;
  }


  /* Timer initialization */
  struct mock_timer_configuration tim_config;
  tim_config.idx = APP_TIM_SERVO;
  tim_config.is_run = MOCK_TIMER_MODE_HALT;
  tim_config.period = (10 * 1000); //sleep for 10ms (slowdown from 0.1us to 10ms for TEST on PC ONLY)
  tim_config.arr_val = 200;
  mock_timer_init(&tim_config); // 20.0ms

  struct servod_configuration servod_test_config;
  servod_test_config.name = "Test";
  servod_test_config.hw_timer_num = 0;
  servod_test_config.ccr_num = 1;
  servod_test_config.tevd = tevd_system;
  servod_test_config.period_us = 20000;
  servod_test_config.sort_in_loop_if_remain_us = 100;
  servod_test_config.sort_timeout_us = 100;
  servod_test_config.max_raw = 2000;
  servod_test_config.event_listener = NULL;
  if (init_servod_test(&servod_test_config) != 0) {
    assert_param(0);
  }

  struct servo_configuration servo_config;
  servo_config.name = "front_left";
  servo_config.id_num = 2;
  servo_config.pin.io_num = 1;
  servo_config.pin.io_num = 4;
  servo_config.min_us = 980;
  servo_config.min_deg = -90;
  servo_config.max_us = 2010;
  servo_config.max_deg = 90;
  if (servod_attach_servo(servod_test, &servo_config) != 0) {
    assert_param(0);
  }
  servod_set_servo_position(servod_test, 2, 45);


  TIMER_START(APP_TIM_SERVO);
  servod_run(servod_test);

  for (;;) {
    servod_poll(servod_test);
    i++;
  }

  return EXIT_SUCCESS;
}


uint32_t
system_get_tick(void)
{
  return time(NULL) - relative_time;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void
assert_failed (const char* expression, uint8_t *file, uint32_t line )
{
  printf("\r\n%s in %s at: %d", expression, file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif

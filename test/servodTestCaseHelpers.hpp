/*
 * servodTestCaseHelpers.hpp
 *
 *  Created on: Apr 13, 2018
 *      Author: Paweł Wójcicki
 */

#ifndef TEST_SERVODTESTCASEHELPERS_HPP_
#define TEST_SERVODTESTCASEHELPERS_HPP_

#include "servo_lib/api.h"

struct servod_configuration* test_get_servod_config();
struct servo_configuration* test_get_servo_config(int servo_id);
int test_get_servo_correct_attach_result(int servo_id);
int test_get_no_servo_configs();


#endif /* TEST_SERVODTESTCASEHELPERS_HPP_ */

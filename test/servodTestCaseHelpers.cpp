/*
 * servodTestCaseHelpers.cpp
 *
 *  Created on: Apr 13, 2018
 *      Author: Paweł Wójcicki
 */


#include <string.h>
#include <iostream>
#include <utest/utest.hpp>
#include "main.hpp"

#include "servodTestCaseHelpers.hpp"
#include "servo_lib/api.h"

using namespace std;
using namespace utest;


struct servod_configuration*
test_get_servod_config()
{
  static struct servod_configuration servod_test_config;
  memset(&servod_test_config, 0, sizeof(struct servod_configuration));

  servod_test_config.name = "Test";
  servod_test_config.hw_timer_num = 0;
  servod_test_config.ccr_num = 1;
  servod_test_config.tevd = tevd_system;
  servod_test_config.period_us = 20000;
  servod_test_config.sort_in_loop_if_remain_us = 100;
  servod_test_config.sort_timeout_us = 100;
  servod_test_config.max_raw = 2000;
  servod_test_config.event_listener = NULL;

  return &servod_test_config;
}

static const struct {
  struct servo_configuration config;
  int is_correct;
} test_servo_set[] = {
  { { 0, (const char* )"Lewy silnik", GPIOA, 2, 1000,2000,-90, 90}, 0 },
  { { 1, (const char* )NULL, GPIOB, 4, 800, 1800, -45, 45 }, 0 },
  { { 2, (const char* )"Zbyt dluga nazwa dla serwa", GPIOD, 14, 1200, 2000, 10, 120 }, 0 },
  { { 3, (const char* )"Smiglo", GPIOE, 5, 3000, 1500, -100, 100 }, -1 },
  { { 5, (const char* )"Wihajster", GPIOA, 10, 200, 2000, 150, -150 }, -1 },
  { { 1, (const char* )"Powtorka", GPIOD, 7, 1002,2002,-91, 91 }, -1 },
  { { 8, (const char* )"Prawy silnik", GPIOC, 3, 200, 2000, -80, 150 }, -1 }
};

struct servo_configuration*
test_get_servo_config(int servo_id)
{
  static struct servo_configuration servo_test_config;
  memset(&servo_test_config, 0, sizeof(struct servo_configuration));
  memcpy(&servo_test_config, &test_servo_set[servo_id], sizeof(struct servo_configuration));

  return &servo_test_config;
}

int
test_get_servo_correct_attach_result(int servo_id)
{
  return test_servo_set[servo_id].is_correct;
}

int
test_get_no_servo_configs()
{
  return NO_ELEMENTS(test_servo_set);
}

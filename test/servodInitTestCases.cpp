/*
 * servoTestCases.cpp
 *
 *  Created on: Mar 28, 2018
 *      Author: Paweł Wójcicki
 */

#include <string.h>
#include <iostream>
#include <utest/utest.hpp>
#include "main.hpp"

#include "servoTestCases.hpp"
#include "servodTestCaseHelpers.hpp"

#include "servo_lib/api.h"
#include "servo_lib/detail/internal.h"

using namespace std;
using namespace utest;


static void test_case_execution(TestCase& test_case) {
    test_case
    .name("dispatcher is initialized").run([] (TestParams& p) {
        const auto& servod = p.context<struct Servod*>();
        TestAssert{p}.not_equal(servod, nullptr);
        TestAssert{p}.equal(strcmp(servod->name, "Test"), 0);
        TestAssert{p}.equal(servod->no_slots, 4);
    })

    .name("servo is attached").run([] (TestParams& p) {
      const auto& servod = p.context<struct Servod*>();
      for (int idx = 0; idx < test_get_no_servo_configs(); idx++) {
	TestAssert{p}.equal(servod_attach_servo(servod, test_get_servo_config(idx)), test_get_servo_correct_attach_result(idx));
      }
    })

    .name("servo is detached").run([] (TestParams& p) {
      const auto& servod = p.context<struct Servod*>();
      servod_attach_servo(servod, test_get_servo_config(1));
      TestAssert{p}.equal(servod_detach_servo(servod, 1), 0);
    });
}

SERVOD_CREATE_STATIC_INSTANCE(static_test, 4);

static TestRunner servod_tests_set([] (TestSuite& test_suite) {
    test_suite.file(__FILE__)
    .fatal(false)

    .name("servod - dynamic allocation").run([] (TestCase& test_case) {
        test_case.setup([] (TestParams& p) {
	    struct Servod* servod_dynamic_test = servod_create_dispatcher_instance(4, malloc);
	    servod_init(servod_dynamic_test, 4, test_get_servod_config());
            p.context(servod_dynamic_test);
        });

        test_case.teardown([] (TestParams& p) {
            free(p.context<struct Servod*>());
        });

        test_case_execution(test_case);
    })

    .name("servod - static allocation").run([] (TestCase& test_case) {
        test_case.context(servod_static_test);

        test_case.setup([] (TestParams& p) {
	    UNUSED(p);
	    init_servod_static_test(test_get_servod_config());
        });

        test_case.teardown([] (TestParams& p) {
	  UNUSED(p);
	  memset(servod_static_test, 0, sizeof(_servod_fixmem_static_test));
        });

        test_case_execution(test_case);
    });
});

int
servo_test_case (void)
{
  return TestStatus::PASS == Test{0, nullptr}.run().status()
          ? EXIT_SUCCESS : EXIT_FAILURE;
}


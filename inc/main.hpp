/*
 * main.h
 *
 *  Created on: Jan 21, 2018
 *      Author: skrucz
 */

#ifndef INC_MAIN_HPP_
#define INC_MAIN_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <iostream>

#include <utest/utest.hpp>
#include <utest/test_extension/evil_defines.hpp>


#include <time.h>
#include <unistd.h>
#include <pthread.h>

#include "common.h"
#include "debug_log.h"
#include "gpio_lib.h"
#include "timer_lib.h"

#include "mock_timer.h"

#include "servo_lib/api.h"

extern tev_dispatcher tevd_system;

EXTERN_SERVOD_STATIC_INSTANCE(test);



#endif /* INC_MAIN_HPP_ */

/*
 * mock_timer.h
 *
 *  Created on: Feb 3, 2018
 *      Author: Paweł Wójcicki
 */

#ifndef _MOCK_TIMER_H_
#define _MOCK_TIMER_H_

#include <stdbool.h>
#include "common.h"

#ifdef __cplusplus
 extern "C" {
#endif

struct mock_timer;

#define NO_TIMERS 8
#define NO_TIMER_CCR 4


enum mock_timer_mode
{
  MOCK_TIMER_MODE_HALT = 0,
  MOCK_TIMER_MODE_RUN
};

struct mock_timer_configuration
{
  uint32_t idx;
  enum mock_timer_mode is_run;
  uint32_t period;
  uint32_t arr_val;
};

struct mock_timer_ccr
{
  bool is_enabled;
  uint32_t value;
};

struct mock_timer
{
  uint8_t idx;
  enum mock_timer_mode mode;
  uint32_t period;
  uint32_t TCR;
  uint32_t ARR;

  struct mock_timer_ccr CCR[NO_TIMER_CCR];
};

extern struct mock_timer dummy_timers[NO_TIMERS];

void mock_timer_cmp_handler (struct mock_timer* timer, uint32_t ccr_num);
void mock_timer_arr_handler (struct mock_timer* timer);

int32_t mock_timer_init (struct mock_timer_configuration* config);
void mock_timer_set_mode (struct mock_timer* timer, enum mock_timer_mode set_run);

#ifdef __cplusplus
}
#endif

#endif /* MOCK_TIMER_H_ */

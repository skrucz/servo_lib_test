/*
 * gpio_lib_dummy.h
 *
 *  Created on: Feb 25, 2017
 *      Author: Paweł Wójcicki
 */

#ifndef _GPIO_LIB_DUMMY_H_
#define _GPIO_LIB_DUMMY_H_

#include <stdint.h>
#include "debug_log.h"

#ifdef __cplusplus
 extern "C" {
#endif

enum {
  GPIOA = 0,
  GPIOB,
  GPIOC,
  GPIOD,
  GPIOE,
  GPIOF,
  NO_GPIO
};

extern const char* dummy_gpio_names[NO_GPIO];

#define GPIO_PIN_SET(GPIO_CTL) DEBUGLOG_LOG(2,"IO SET: %s%d", dummy_gpio_names[GPIO_CTL.io_num], GPIO_CTL.pin_mask)
#define GPIO_PIN_CLEAR(GPIO_CTL) DEBUGLOG_LOG(2,"IO CLR: %s%d", dummy_gpio_names[GPIO_CTL.io_num], gGPIO_CTLpio_pin.pin_mask)

#define GPIO_SET_MASK(GPIO_CTL) DEBUGLOG_LOG(2,"IO MASK SET: %s%d", dummy_gpio_names[GPIO_CTL.io_num], GPIO_CTL.pin_mask)
#define GPIO_CLR_MASK(GPIO_CTL) DEBUGLOG_LOG(2,"IO MASK CLR: %s%d", dummy_gpio_names[GPIO_CTL.io_num], GPIO_CTL.pin_mask)


#ifdef __cplusplus
}
#endif

#endif /* _GPIO_LIB_F1_H_ */

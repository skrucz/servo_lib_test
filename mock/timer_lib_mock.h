/*
 * timer_lib_dummy.h
 *
 *  Created on: Mar 18, 2018
 *      Author: Paweł Wójcicki
 */

#ifndef _TIMER_LIB_MOCK_H_
#define _TIMER_LIB_MOCK_H_

#include "mock_timer.h"

#define TIMER_START(T_NUM) DEBUGLOG_LOG(2,"TIM%d: START", T_NUM); mock_timer_set_mode(&dummy_timers[T_NUM], MOCK_TIMER_MODE_RUN)
#define TIMER_STOP(T_NUM) DEBUGLOG_LOG(2,"TIM%d: STOP", T_NUM); mock_timer_set_mode(&dummy_timers[T_NUM], MOCK_TIMER_MODE_HALT)
#define TIMER_RESET(T_NUM) DEBUGLOG_LOG(2,"TIM%d: RESET", T_NUM); dummy_timers[T_NUM].TCR = 0

#define TIMER_ENABLE_CCR(T_NUM, CCR_NUM) DEBUGLOG_LOG(2,"TIM%d, CCR%d: ENABLE", T_NUM, CCR_NUM); dummy_timers[T_NUM].CCR[CCR_NUM].is_enabled = true
#define TIMER_DISABLE_CCR(T_NUM, CCR_NUM) DEBUGLOG_LOG(2,"TIM%d, CCR%d: DISABLE", T_NUM, CCR_NUM); dummy_timers[T_NUM].CCR[CCR_NUM].is_enabled = false
#define TIMER_SET_CCR(T_NUM, CCR_NUM, VAL) DEBUGLOG_LOG(2,"TIM%d, CCR%d = %d", T_NUM, CCR_NUM, VAL); dummy_timers[T_NUM].CCR[CCR_NUM].value = VAL
#define TIMER_INCREMENT_CCR(T_NUM, CCR_NUM, VAL) DEBUGLOG_LOG(2,"TIM%d, CCR%d += %d", T_NUM, CCR_NUM, VAL); dummy_timers[T_NUM].CCR[CCR_NUM].value += VAL

#define TIMER_RESET_CCR(T_NUM, CCR_NUM) DEBUGLOG_LOG(2,"TIM%d, CCR%d = %d", T_NUM, CCR_NUM, 0); dummy_timers[T_NUM].CCR[CCR_NUM].value = 0

#endif /* _TIMER_LIB_DUMMY_H_ */

/*
 * mock_timer.c
 *
 *  Created on: Feb 3, 2018
 *      Author: Paweł Wójcicki
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include "debug_log.h"

#include "mock_timer.h"

struct mock_timer dummy_timers[NO_TIMERS];

static void* threadMockTimer (void* pv_timer);

__weak void
mock_timer_cmp_handler (struct mock_timer* timer, uint32_t ccr_num)
{
  UNUSED(timer); UNUSED(ccr_num);
}

__weak void
mock_timer_arr_handler (struct mock_timer* timer)
{
  UNUSED(timer);
}

static void*
threadMockTimer (void* pv_timer)
{
  struct mock_timer* timer = (struct mock_timer* )pv_timer;
  for (;;) {
    if (timer->mode == MOCK_TIMER_MODE_RUN) {
      timer->TCR++;
      if (timer->TCR >= timer->ARR) {
	timer->TCR = 0;
	DEBUGLOG_LOG(3, "T%d, ARR", timer->idx);
	mock_timer_arr_handler(timer);
      }

      for (uint32_t i = 0; i < NO_TIMER_CCR; i++) {
	if (timer->CCR[i].is_enabled == false) {
	  continue;
	}

	if (timer->TCR == timer->CCR[i].value) {
	  DEBUGLOG_LOG(3, "T%d, CCR%d: %d", timer->idx, i, timer->CCR[i].value);
	  mock_timer_cmp_handler(timer, i);
	}
      }
    }

    usleep(timer->period); //sleep for timer period time
  }

  return NULL;
}

void
mock_timer_set_mode (struct mock_timer* timer, enum mock_timer_mode set_run)
{
  timer->mode = set_run;
}

int32_t
mock_timer_init (struct mock_timer_configuration* config)
{
  assert_param(config);
  assert_param(config->idx < NO_TIMERS);

  dummy_timers[config->idx].idx = config->idx;
  dummy_timers[config->idx].mode = config->is_run;
  dummy_timers[config->idx].period = config->period;
  dummy_timers[config->idx].ARR = config->arr_val;

  // create the thread, pass the reference, address of the function and data
  // pthread_create() returns 0 on the successful creation of a thread
  pthread_t thread;
  if (pthread_create(&thread, NULL, &threadMockTimer, &dummy_timers[config->idx]) != 0) {
    puts("\r\nFailed to create the thread");
    assert_param(0);
    return -1;
  }

  return 0;
}

CONFIG_UTILS_TIME_EVENT=y
#CONFIG_UTILS_CLI=y
CONFIG_XITOA_ONLY_STDFUNC=y

include $(COMMON_LIBS)/Utils.mk
include $(COMMON_LIBS)/Debuglog.mk

include $(THIRD_PARTY)/libmbb/Rules.mk
INCDIR += $(THIRD_PARTY)

CONFIG_SERVO_LIB_IRQ_EXECUTABLE_TIME_IN_US=30
include servo_lib/Rules.mk

include $(COMMON_LIBS)/OS.mk
